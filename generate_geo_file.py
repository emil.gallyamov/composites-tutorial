#!/usr/bin/env python

import numpy as np

# INPUT PARAMETERS:
aggregates_file = 'circle_coord.csv'


################################################################################

# PRINT LIST ------------------------------------------------------
def printList(output_file, data, name, el_size=False, label=None):
    if label == None:
        label = range(1, len(data)+1)

    for i in range(len(data)):
        output_file.write(name+'('+str(label[i])+') = {')
        for s in data[i][:-1]:
            output_file.write(repr(s)+', ')
        output_file.write(repr(data[i][-1]))
        if el_size:
            output_file.write(', element_size')
        output_file.write('};\n')
    output_file.write('\n')

# PRINT ------------------------------------------------------------


def printFile(file_name, points, lines, circles, line_loops, element_size, is_3D):

    output_file = open(file_name, 'w')
    output_file.write('element_size = '+repr(element_size)+';\n\n')

    printList(output_file, points, 'Point', True)
    printList(output_file, lines, 'Line')
    printList(output_file, circles, 'Circle', False,
              range(len(lines)+1, len(lines)+len(circles)+1))
    printList(output_file, line_loops, 'Line Loop')

    if is_3D:
        # this are the surfaces of the cube
        plane_surfaces = [[i] for i in range(1, 7)]
        printList(output_file, plane_surfaces, 'Plane Surface')

        # surfaces of the spheres
        ruled_surfaces = [[i] for i in range(7, len(line_loops) + 1)]
        printList(output_file, ruled_surfaces, 'Ruled Surface',
                  False, range(7, len(line_loops) + 1))

        # surface loops
        surface_loops = [range(i*8+7, i*8+15) for i in range(len(circles)/12)]
        printList(output_file, surface_loops, 'Surface Loop')
        printList(output_file, [range(1, 7)],
                  'Surface Loop', False, [len(surface_loops) + 1])

        # aggregate volumes
        aggregate_volumes = [[i] for i in range(1, len(surface_loops) + 1)]
        printList(output_file, aggregate_volumes, 'Volume')

        # paste volume
        printList(output_file, [
                  [len(surface_loops)+1]+range(-len(surface_loops), 0)], 'Volume', False, [len(surface_loops)+1])

        # aggregate physical volume
        printList(output_file, [range(1, len(surface_loops)+1)],
                  'Physical Volume', False, ['"inclusions"'])

        # paste physical volume
        printList(output_file, [[len(surface_loops)+1]],
                  'Physical Volume', False, ['"matrix"'])

    else:
        # aggregates plane surfaces
        plane_surfaces = [[i] for i in range(2, len(line_loops)+1)]
        printList(output_file, plane_surfaces, 'Plane Surface')

        # paste plane surface
        printList(output_file, [[1]+list(range(-len(line_loops), -1))],
                  'Plane Surface', False, [len(plane_surfaces)+1])

        # aggregates physical surface
        printList(output_file, [range(1, len(plane_surfaces)+1)],
                  'Physical Surface', False, ['"inclusions"'])

        # paste physical surface
        printList(output_file, [[len(plane_surfaces) + 1]],
                  'Physical Surface', False, ['"matrix"'])

        # print the physical lines to apply boundary conditions
        printList(output_file,
                  [[i] for i in range(1, len(lines) + 1)],
                  'Physical Line', False,
                  ['"top"', '"left"', '"bottom"', '"right"'])

        output_file.close()

# AGGREGATES -------------------------------------------------------


def generateAggregates(points, circles, line_loops, aggregate_data):
    for a in aggregate_data:
        r = a[0] / 2
        c = a[1:]
        spatial_dimension = len(c)
        if spatial_dimension == 2:
            c = np.concatenate((c, [0]))

        center_index = len(points) + 1

        # adding center and the border points
        points.append(np.copy(c))

        for sign in [1, -1]:
            for dim in range(spatial_dimension):
                points.append(np.copy(c))
                points[-1][dim] += sign * r

        if spatial_dimension == 3:
            circle_start_index = len(circles) + 12
            addCircle(circles, center_index, [1, 2, 4, 5])
            addCircle(circles, center_index, [1, 6, 4, 3])
            addCircle(circles, center_index, [2, 6, 5, 3])

            addLineLoop(line_loops, [1, -12, 8], circle_start_index)
            addLineLoop(line_loops, [1, 9, -5], circle_start_index)
            addLineLoop(line_loops, [2, 7, 12], circle_start_index)
            addLineLoop(line_loops, [2, -6, -9], circle_start_index)

            addLineLoop(line_loops, [-8, -11, 4], circle_start_index)
            addLineLoop(line_loops, [5, 10, 4], circle_start_index)
            addLineLoop(line_loops, [-7, 3, 11], circle_start_index)
            addLineLoop(line_loops, [3, -10, 6], circle_start_index)
        else:
            addCircle(circles, center_index, range(1, 5))
            addLineLoop(line_loops, list(range(1, 5)), len(line_loops)*4)


def addCircle(circles, center_index, connectivity):
    for i in range(4):
        circles.append([center_index + connectivity[i],
                        center_index,
                        center_index + connectivity[(i+1) % 4]])


def addLineLoop(line_loops, connectivity, start_index=0):
    for i in range(len(connectivity)):
        if connectivity[i] > 0:
            connectivity[i] += start_index
        else:
            connectivity[i] -= start_index
    line_loops.append(connectivity)


# CUBE ----------------------------------------------------------
def generateCube(points, lines, line_loops, side):
    for sign_z in [1, -1]:
        for sign_y in [1, -1]:
            for sign_x in [1, -1]:
                points.append([side/2 * sign_x,
                               side/2 * sign_y,
                               side/2 * sign_z])

    # front face
    lines.append([1, 2])
    lines.append([2, 4])
    lines.append([4, 3])
    lines.append([3, 1])
    addLineLoop(line_loops, [1, 2, 3, 4])

    # back face
    lines.append([5, 6])
    lines.append([6, 8])
    lines.append([8, 7])
    lines.append([7, 5])
    addLineLoop(line_loops, [5, 6, 7, 8])

    # other faces
    lines.append([1, 5])
    lines.append([2, 6])
    lines.append([3, 7])
    lines.append([4, 8])
    addLineLoop(line_loops, [-1, 9, 5, -10])
    addLineLoop(line_loops, [2, 12, -6, -10])
    addLineLoop(line_loops, [3, 11, -7, -12])
    addLineLoop(line_loops, [4, 9, -8, -11])

# SQUARE ----------------------------------------------------------


def generateSquare(points, lines, line_loops, side):
    points.append([side/2,  side/2, 0])
    points.append([-side/2,  side/2, 0])
    points.append([-side/2, -side/2, 0])
    points.append([side/2, -side/2, 0])

    lines.append([1, 2])
    lines.append([2, 3])
    lines.append([3, 4])
    lines.append([4, 1])

    addLineLoop(line_loops, list(range(1, 5)))

# MAIN ------------------------------------------------------------


def main(cube_length, element_size, output_file):
    aggregate_data = np.loadtxt(aggregates_file, delimiter=',')
    if len(aggregate_data.shape) == 1:
        aggregate_data = aggregate_data.reshape(1,aggregate_data.shape[0])
    is_3D = aggregate_data.shape[1] == 4

    points = []
    lines = []
    line_loops = []
    circles = []

    if is_3D:
        generateCube(points, lines, line_loops, cube_length)
    else:
        generateSquare(points, lines, line_loops, cube_length)
    generateAggregates(points, circles, line_loops, aggregate_data)
    printFile(output_file, points, lines, circles,
              line_loops, element_size, is_3D)


if __name__ == "__main__":
    main()
