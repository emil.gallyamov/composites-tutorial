import numpy as np
import matplotlib.pyplot as plt
import sys


def gen_mesh(d_agg, vp, L):

    # packing density
    #vp = 0.7
    # box size (72.8mm for concrete, 40mm for mortar)
    #L = 70
    #H = 70
    # distribution factor
    gamma = 0.
    # spatial dimension
    dim = 2
    max_iter = 200000
    max_rot_iter = 0
    eps = 1e-10

    # generate the volume of aggregates to be generated in each grading segment
    ng = []  # number of generated particles
    Ar = 0.
    dp = []
    A = L * L

    def volume(diameter):
        if dim == 2:
            return np.pi * (diameter / 2)**2
        return 4./3 * np.pi * (diameter / 2)**3

    while A * vp > Ar:
        dp.append(d_agg)
        Ar += volume(d_agg)

    # Check the packing density
    print('Calculated packing density: {:.2f}'.format(Ar/A))
    print('Total number of circles: {}'.format(len(dp)))

    # sort the aggregates before placing them
    dp.sort(reverse=True)

    # Now, these circles are need to be placed to the appropriate coord.
    # 1st condition: Stay in the boundary of the circle (Can be done directly
    # with setting xmin and xmax related to the diameter)
    # 2nd condition: Do not overlap with other circles
    # 3rd condition: Coat with mortar film (Done together with 2)
    # Hence, only condition 2 is to be repeatedly satisfied

    def get_new_center(limit_x, limit_y):
        center = []
        center.append(np.random.uniform(-limit_x, limit_x))
        center.append(np.random.uniform(-limit_y, limit_y))
        return center

    gen_mesh.rot_it = 0

    def rotate_aggregate(limit_x, limit_y, center, dist):
        dist += eps
        while gen_mesh.rot_it < max_rot_iter:
            gen_mesh.rot_it += 1
            if len(center) == 2:
                angle = np.random.uniform(0, 2*np.pi)
                angle_vec = np.array([np.cos(angle), np.sin(angle)])
            else:
                theta = np.random.uniform(0, 2*np.pi)
                phi = np.random.uniform(0, 2*np.pi)
                angle_vec = np.array(
                    [np.cos(theta)*np.sin(phi), np.sin(theta)*np.cos(phi), np.cos(theta)])

            new_pos = center + angle_vec * dist
            if (new_pos[0] > -limit_x) and (new_pos[0] < limit_x) \
                    and (new_pos[1] > -limit_y) and (new_pos[1] < limit_y):
                return new_pos
        return None

    def find_overlap(new_x, past_x, min_dist):
        # copy the new center for every sphere already placed
        new_x_repeated = np.ones_like(past_x) * new_x

        # calculate difference
        diff = past_x - new_x_repeated

        # calculate distance
        dist = np.linalg.norm(diff, axis=1)

        # is the new sphere too close to the already inserted ones?
        is_too_close = dist < min_dist

        # find overlaps
        overlaps = np.nonzero(is_too_close)[0]

        if len(overlaps) == 0:
            return (False, None)

        return (True, overlaps[0])

    dp = np.array(dp)
    limit_x = L/2 - dp[0] / 2 * (1+2*gamma)
    limit_y = L/2 - dp[0] / 2 * (1+2*gamma)
    # fill already first center
    x = [get_new_center(limit_x, limit_y)]

    for i in range(1, len(dp)):
        limit_x = L/2 - dp[i] / 2 * (1+2*gamma)
        limit_y = L/2 - dp[i] / 2 * (1+2*gamma)
        past_x = np.array(x)

        # calculate minimal distance
        min_dist = (dp[:i] + dp[i]) / 2 + gamma * dp[i]

        # set new center for the first try
        new_x = get_new_center(limit_x, limit_y)

        rot_it = 0
        intersected_agg = None
        it = 0

        while it < max_iter:
            overlap = find_overlap(new_x, past_x, min_dist)

            if overlap[0] == False:
                print(
                    'Sphere {} is successfully placed after {} iterations'.format(i, it+1))
                x.append(new_x)
                break
            else:
                if intersected_agg == None:
                    intersected_agg = overlap[1]
                new_x = rotate_aggregate(
                    limit_x, limit_y, x[intersected_agg], min_dist[intersected_agg])

            if new_x == None:
                new_x = get_new_center(limit_x, limit_y)
                it += 1
                rot_it = 0
                intersected_agg = None
        else:
            sys.exit('Inclusion could not be placed')

    # save the data
    data = np.hstack((dp[:, None], x))
    title = 'Diameter, x, y'
    if dim == 3:
        title += ', z'
    np.savetxt('circle_coord.csv', data, delimiter=',', header=title)

    if dim == 2:
        # final figure with the aggregates
        fig = plt.gcf()
        plt.axes().set_aspect('equal')
        for i in range(len(x)):
            circle = plt.Circle(x[i], radius=dp[i]/2)
            fig.gca().add_artist(circle)

        plt.axis([-L/2, L/2, -L/2, L/2])
        plt.show()

def gen_mesh_mult(d_max, d_min, vp, L):

    # exponent of Fuller curve
    n = 0.5 # value used by Wriggers
    # distribution factor
    gamma = 0.2;
    # spatial dimension
    dim = 2
    max_iter = 100000
    max_rot_iter = 0
    eps = 1e-10

    def pfuller( d ):
        return 100 * ( d / d_max ) **n

    def plinear( d ):
        return 100 * ( (d - d_min) / (d_max - d_min))

    d = np.linspace(d_max, d_min, num = 10)
    p = []

    # grading segments from my head:
    #d.append(d_max)

    for i in range(len(d)):
        p.append(plinear(d[i]))


    # generate the volume of aggregates to be generated in each grading segment
    ng = [] # number of generated particles
    A_exact = 0.
    A_approx = 0.
    Ar = 0.
    dp = []
    A = L**2
    if dim == 3: A = L**3

    def volume( diameter ):
        if dim == 2:
            return np.pi * ( diameter / 2 )**2
        return 4./3 *np.pi * ( diameter / 2 )**3

    for i in range( len(d) - 1 ): # put minus one to have nb of grading intervals
        Ap = ( plinear( d[i] ) - plinear( d[i+1] ) ) / ( plinear( d_max ) - plinear( d_min ) ) * vp * A

        A_exact += Ap
        Ar += Ap
        ng.append(0)

        while Ar >= volume( d[i+1] ):
            d_agg = np.random.uniform( d[i+1], d[i] )
            dp.append( d_agg )
            Ap = volume ( d_agg )
            A_approx += Ap
            Ar -= Ap
            ng[-1] += 1

    # Check the packing density
    A_meso = A_exact - Ar;
    print ('Calculated packing density: {:.2f}'.format(A_meso/A));
    print ('Total number of circles: {}'.format( len(dp) ));

    # sort the aggregates before placing them
    dp.sort( reverse=True );

    # Now, these circles are need to be placed to the appropriate coord.
    # 1st condition: Stay in the boundary of the circle (Can be done directly
    # with setting xmin and xmax related to the diameter)
    # 2nd condition: Do not overlap with other circles
    # 3rd condition: Coat with mortar film (Done together with 2)
    # Hence, only condition 2 is to be repeatedly satisfied

    def get_new_center( limit ):
        return np.random.uniform(-limit, limit, dim)

    gen_mesh_mult.rot_it = 0
    def rotate_aggregate( limit, center, dist ):
        dist += eps
        while gen_mesh_mult.rot_it < max_rot_iter:
            gen_mesh_mult.rot_it += 1
            if len(center) == 2:
                angle = np.random.uniform( 0, 2*np.pi )
                angle_vec = np.array([np.cos(angle), np.sin(angle)])
            else:
                theta = np.random.uniform( 0, 2*np.pi )
                phi = np.random.uniform( 0, 2*np.pi )
                angle_vec = np.array([np.cos(theta)*np.sin(phi), np.sin(theta)*np.cos(phi), np.cos(theta)])

            new_pos = center + angle_vec * dist
            if np.all(new_pos > -limit) and np.all(new_pos < limit):
                return new_pos
        return None

    def find_overlap( new_x, past_x, min_dist ):
        # copy the new center for every sphere already placed
        new_x_repeated = np.ones_like( past_x ) * new_x

        # calculate difference
        diff = past_x - new_x_repeated

        # calculate distance
        dist = np.linalg.norm( diff, axis=1 )

        # is the new sphere too close to the already inserted ones?
        is_too_close = dist < min_dist

        # find overlaps
        overlaps = np.nonzero( is_too_close )[0]

        if len( overlaps ) == 0:
            return ( False, None )

        return ( True, overlaps[0] )


    dp = np.array( dp )
    limit = L/2 - dp[0] / 2* (1+2*gamma)
    # fill already first center
    x = [get_new_center( limit )]

    for i in range( 1, len(dp) ):
        limit = L/2 - dp[i] / 2* (1+2*gamma)
        past_x = np.array( x )

        # calculate minimal distance
        min_dist = (dp[:i] + dp[i]) / 2 + gamma * dp[i]

        # set new center for the first try
        new_x = get_new_center( limit )

        rot_it = 0
        intersected_agg = None
        it = 0

        while it < max_iter:
            overlap = find_overlap( new_x, past_x, min_dist )

            if overlap[0] == False:
                print ('Sphere {} is successfully placed after {} iterations'.format(i,it+1))
                x.append( new_x )
                break
            else:
                if intersected_agg == None: intersected_agg = overlap[1]
                new_x = rotate_aggregate( limit, x[intersected_agg], min_dist[intersected_agg] )

            if new_x == None:
                new_x = get_new_center( limit )
                it += 1
                rot_it = 0
                intersected_agg = None
        else:
            sys.exit('Aggregate could not be placed')

    # save the data
    data = np.hstack( (dp[:,None], x) )
    title = 'Diameter, x, y'
    if dim == 3: title += ', z'
    np.savetxt( 'circle_coord.csv', data, delimiter=',', header=title )


    if dim == 2:
        # final figure with the aggregates
        plt.axes().set_aspect('equal')
        fig = plt.gcf()

        for i in range(len(x)):
            circle = plt.Circle( x[i], radius=dp[i]/2 )
            fig.gca().add_artist( circle )

        plt.axis([-L/2, L/2, -L/2, L/2])
        plt.show()
